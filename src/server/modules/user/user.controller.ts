/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route-async-wrapper";
import {connection as db} from "../../db/mysql-connection";
import {OkPacket,RowDataPacket} from "mysql2/promise";
import express, { Request,Response } from "express";
import log from "@ajar/marker";
import * as user_service from "./user.service"
import { nextTick } from "process";
import { Agent, validate_user, validate_place, Place } from "../../types/type.model"



const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW Agent
router.post("/agent",raw(async (req:Request, res:Response) => {
    log.obj(req.body, "create a agent, req.body:");

    const valid = validate_user(req.body);
    if(!valid && validate_user.errors) res.json( {status:401, error: validate_user.errors[0]} );
    else{

    const result = await user_service.add_new_user(req.body as Agent);
    
    const ok = {status:200,message:`User Created successfully`};
    const fail = {status:404,message:`Error in creating user `};
    const {status,message } = result.affectedRows  ? ok : fail;
    res.status(status).json({message,result});
    }
  })
);

// CREATES A NEW Place
router.post("/place",raw(async (req:Request, res:Response) => {
    log.obj(req.body, "create a place");
  
    const valid = validate_user(req.body);
    if(!valid && validate_place.errors) res.json( {status:401, error: validate_place.errors[0]} );
    else{

    const result = await user_service.add_new_place(req.body as Place);
    
    const ok = {status:200,message:`User Created successfully`};
    const fail = {status:404,message:`Error in creating user `};
    const {status,message } = result.affectedRows  ? ok : fail;
    res.status(status).json({message,result});
    }
  })
);


// GET ALL USERS
router.get("/",raw(async (req:Request, res:Response) => {
    
    // const sql = `SELECT * FROM users`;
    // const [rows] = await db.query(sql);
    const rows = await user_service.get_all_users();
    // if(!(rows.serviceErr)){
    res.status(200).json(rows);
    // } else {
    //   res.status(401).json(rows.serviceErr);
    // }
  })
);


// router.get("/:id/comments",raw(async (req:Request, res:Response) => {
    
//     // const sql = `SELECT * FROM users`;
//     // const [rows] = await db.query(sql);
//     const rows = await user_service.get_user_comments(Number(req.params.id));
//     // if(!(rows.serviceErr)){
//     res.status(200).json(rows);
//     // } else {
//     //   res.status(401).json(rows.serviceErr);
//     // }
//   })
// );

// router.get('/paginate/:page?/:items?', raw( async(req:Request, res:Response)=> {

//   log.obj(req.params, "paginate, req.params:");
//   let { page = '0' ,items = '10' } = req.params;


//     const sql = `
//       SELECT * FROM users 
//       LIMIT ${parseInt(items)} 
//       OFFSET ${parseInt(page) * parseInt(items)}`;
//     const [rows] = await db.query(sql);
//     res.status(200).json(rows);   

// }))


// GETS A SINGLE Agent
router.get("/:id",raw(async (req:Request, res:Response) => {
    // const sql = `SELECT * FROM users WHERE id = ${req.params.id}`;
    // console.log('in get-single', sql);
    
    // const results:RowDataPacket = await db.query(sql) as RowDataPacket;
    // console.log('in get-single', results[0][0]);
    const user = await user_service.get_single_user(Number(req.params.id));

    // const user = results[0][0][0][0] = await db.query(sql);
    // console.log('in get-single', user);
    
    if (!user) {
       res.status(404).json({ message: `No user found. with id of ${req.params.id}` });
       return
    }
    res.status(200).json(user);
  })
);

// UPDATES A SINGLE USER
// router.put("/:id",raw(async (req:Request, res:Response) => { 

//     // const updates =  Object.entries(req.body).map(([key])=>`${key}=?`) 
//     // console.log('in update', updates);
    
//     // const sql = `UPDATE users SET ${updates} WHERE id='${req.params.id}'`;
//     // // log.yellow(sql);
//     // const results = await db.query(sql, Object.values(req.body));
//     // const result:OkPacket = results[0] as OkPacket;
//     // const valid = validate_user_update(req.body);
//     // if(!valid && validate_user_update.errors) res.json({status: 401,error: validate_user_update.errors[0]});
//     // else{

//     const result = await user_service.update_single_user(Number(req.params.id), req.body);
//     const ok = {status:200,message:`User ${req.params.id} updated successfully`};
//     const fail = {status:404,message:`Error in updating user ${req.params.id}`};
//     const {status,message} = result.affectedRows ? ok : fail;
//     res.status(status).json({message});
//     // }
//   })
// );

// DELETES A USER
// router.delete("/:id",raw(async (req:Request, res:Response) => {
//     // var sql = `DELETE FROM users WHERE id=?`;
//     // const results = await db.query(sql, [req.params.id]);
//     // const result:OkPacket = results[0] as OkPacket;
//     const result = await user_service.delete_single_user(Number(req.params.id));
//     const ok = {status:200,message:`User ${req.params.id} deleted successfully`};
//     const fail = {status:404,message:`Error in deleting user ${req.params.id}`};
//     const {status,message} = result.affectedRows ? ok : fail;
//     res.status(status).json({message});
//   })
// );


export default router;
