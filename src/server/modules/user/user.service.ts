import {OkPacket,RowDataPacket} from "mysql2/promise";
import {connection as db} from "../../db/mysql-connection";
import log from "@ajar/marker";
import {Agent, Place} from "../../types/type.model" 

export const mytest = ()=> {
    log.green('just testing..');

}

// Get all users from DB
export const get_all_users = async()=>{
    console.log('in get all users - service');
    // try {
        // const sql = `SELECT * FROM agents`;
        const sql = `
        Select name, type, owner, status, places.name_place as place_name, places.hits as hits, agents.id as id_agent 
            from agents 
            LEFT JOIN places ON
	    agents.id = places.id_agent;
        `
        const [rows] = await db.query(sql);
        console.log('in get all users:', rows);
        return rows;

    // } catch(serviceErr) {
    //     log.red('in service',serviceErr);
    //     return { serviceErr };
    // }
}

// Adds new Agent/user to DB
export const add_new_user = async(data: Agent): Promise<OkPacket>=> {
    console.log('in add new user - service');
    
    const sql = `INSERT INTO agents SET ?`;
    const results = await db.query(sql, data);
    const result:OkPacket = results[0] as OkPacket;
    return result;
}

// Adds new Place to DB
export const add_new_place = async(data: Place): Promise<OkPacket>=> {
    console.log('in add new place - service');
    
    const sql = `INSERT INTO places SET ?`;
    const results = await db.query(sql, data);
    const result:OkPacket = results[0] as OkPacket;
    return result;
}

// Get specific user by id
export const get_single_user = async(id: number): Promise<OkPacket>=> {
    console.log('in get single user - service');

    // const sql = `SELECT * FROM agents WHERE id = ${id}`;
    const sql = `
    Select name, type, owner, status, places.name_place as place_name, places.hits as hits 
    from agents 
    LEFT JOIN places ON
        agents.id = places.id_agent
    where agents.id = ${id};
    `
    // console.log('in get-single', sql);
    const results:RowDataPacket = await db.query(sql) as RowDataPacket;
    console.log('in get single - id', results[0]);
    return results[0];
}

export const update_single_user = async(id: number, data: object)=> {
    console.log('in update single user - service');

    const updates =  Object.entries(data).map(([key])=>`${key}=?`) 
    console.log('in update', updates);
    
    const sql = `UPDATE agents SET ${updates} WHERE id='${id}'`;
    // log.yellow(sql);
    const results = await db.query(sql, Object.values(data));
    const result:OkPacket = results[0] as OkPacket;
    return result;
    
}

export const delete_single_user = async(id: number)=> {
    console.log('in delete single user - service');

    var sql = `DELETE FROM agents WHERE id=?`;
    const results = await db.query(sql, [id]);
    const result:OkPacket = results[0] as OkPacket;
    return result;
    
}

// export const get_user_comments = async(id: number): Promise<RowDataPacket>=> {
//     console.log('in get single user - service');

//     const sql = `
//     SELECT * FROM agents 
//     JOIN comments ON 
//         agents.id = comments.id_user_suggested
//     WHERE agents.id = ${id}`;
//     // console.log('in get-single', sql);
//     const results:RowDataPacket = await db.query(sql) as RowDataPacket;
//     console.log('in get single - id', results[0]);
//     return results[0];

// }