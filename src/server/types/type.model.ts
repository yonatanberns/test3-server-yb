
import Ajv, {JTDSchemaType} from "ajv/dist/jtd"
const ajv = new Ajv()


enum eType {
    Human = "Human",
    Android = "Android",
    Sensor = "Sensor",
    Machine = "Machine"
  }

export interface Agent {
    name: string;
    type: string;
    owner?: string;
    status: string;
  }

export interface Place {
    name_place: string;
    hits: number;
    id_agent: number
}  

const user_schema: JTDSchemaType<Agent> = {
    properties: {
        name: {type: "string"},
        type: {type: "string"},
        status: {type: "string"}
    },
    optionalProperties: {
        owner: {type: "string"}
    }
}

const place_schema: JTDSchemaType<Place> = {
    properties: {
        name_place: {type: "string"},
        id_agent: {type: "int32"},
        hits: {type: "int32"}
    }
}


// validate_user is a type guard for Agent - type is inferred from schema type
export const validate_user = ajv.compile(user_schema)
export const validate_place = ajv.compile(place_schema)